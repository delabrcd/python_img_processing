from p2.cImage import *


# stenago(imageFileName, secret)
# Encodes a secret message into a picture by using the last 4 bits of the red and green pixel information
# stenago uses the 3rd row of pixels in an image to store its content
# Parameters:
# imageFileName: string file path to .gif file to be encoded
# secret: text to be encoded in the image
# return: a cimage object of the encoded image

def stenago(imageFileName, secret):
    image = FileImage(imageFileName)
    y = image.getHeight()
    x = image.getWidth()
    for px in range(x):
        for py in range(y):
            pixel = image.getPixel(px, py)
            pixel.red &= ~0xf
            pixel.blue &= ~0xf
            pixel.green &= ~0xf
            image.setPixel(px, py, pixel)
    for i in range(len(secret)):
        cha = int(hex(ord(secret[i])), 16)
        high = (cha & 0xf0) >> 4
        low = (cha & 0x0f)
        pix = image.getPixel(i, 2)
        gr = format(pix.green, '08b')
        pix.red |= low
        rd = format(pix.red, '08b')
        pix.green |= high
        image.setPixel(i, 2, pix)
    return image

# reverseStenago(imageFileName)
# Takes an image encoded by the stenago function and decodes the text in it
# Parameters:
# imageFileName: string file path to .gif file to be decoded
# return: a string with the decoded text

def reverseStenago(imageFileName):
    image = FileImage(imageFileName)
    y = image.getHeight()
    x = image.getWidth()
    secret = ""
    for px in range(x):
        pixel = image.getPixel(px, 2)
        upper = format(pixel.green, '08b')
        lower = format(pixel.red, '08b')
        upper = upper[4:8]
        lower = lower[4:8]
        complete = upper + lower
        c = chr(int(complete, 2))
        if complete != "00000000":
            secret = secret + c
        else:
            return secret


if __name__ == "__main__":
    title1 = 'images/redEyes1.gif'
    oldImage = FileImage(title1)
    newImage = stenago(title1, "hello")
    newImage.saveTk("new")
    print(reverseStenago("new.gif"))
