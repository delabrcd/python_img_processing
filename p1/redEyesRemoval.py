from p1.cImage import *


# removeRedEyes(imageFileName, col1, row1, col2, row2)
# Takes a path to a gif image, the coordinates of two opposite corners of a box where there are red eyes and
# removes the red eyes
# Parameters:
# imageFileName: string file path to a gif image
# col1: x1 coordinate of one box corner
# row1: y1 coordinate of one box corner
# col2: x2 coordinate of one box corner
# row2: y2 coordinate of one box corner
# returns: cimage object of the adjusted image

def removeRedEyes(imageFileName, col1, row1, col2, row2):
    oldImage = FileImage(imageFileName)
    newImage = oldImage
    xDistance = col2 - col1
    yDistance = row2 - row1
    (xFactor, yFactor) = factor(xDistance, yDistance)
    for dx in range(xDistance * xFactor):
        for dy in range(yDistance * yFactor):
            pixelX = col1 + dx * xFactor
            pixelY = row1 + dy * yFactor
            colors = oldImage.getPixel(pixelX, pixelY)
            if (
                    colors.red > 130 and (
                    colors.blue < 90 or colors.green < 90)):
                print(colors)
                colors.red = (colors.green + colors.blue) // 2
                print("color changed", colors)
            newImage.setPixel(pixelX, pixelY, colors)
    return newImage


def factor(x, y):
    if x > 0:
        tempX = 1
    elif x < 0:
        tempX = -1
    else:
        tempX = 0
    if y > 0:
        tempY = 1
    elif y < 0:
        tempY = -1
    else:
        tempY = 0
    return (tempX, tempY)


def drawOldImage(oldImage, title):
    imageFileName = title
    win = ImageWin(imageFileName + " red eye correction", oldImage.getWidth() * 2, oldImage.getHeight())
    oldImage.draw(win)
    return win


def drawNewImage(win, newImage):
    newImage.setPosition(newImage.getWidth(), 0)
    newImage.draw(win)
    print("Click the image again to exit")
    win.exitOnClick()


def getChords(win):
    checkChords = False
    print("Please click on the image the corners of the area that you would like to remove the red from")
    # while checkChords is not True:
    pos1 = win.getMouse()
        # print("Corner 1 selected at: ", pos1, " to undo select the other corner outside of the image")
    pos2 = win.getMouse()
        # print("Corner 2 selected at: ", pos2)
        # if pos1[0] < 400 and pos1[1] < 400 and pos2[0] < 400 and pos2[1] < 400:
        #     checkChords = True
        # else:
        #     print("Invalid coordinates entered, please select both corners of the rectangle again")

    return (pos1[0], pos1[1], pos2[0], pos2[1])


if __name__ == '__main__':
    title1 = input("Enter File Path:")
    oldImage = FileImage(title1)
    win = drawOldImage(oldImage, title1)
    chords = getChords(win)  # get the coordinates
    newImage = removeRedEyes(title1, chords[0], chords[1], chords[2], chords[3])
    drawNewImage(win, newImage)
